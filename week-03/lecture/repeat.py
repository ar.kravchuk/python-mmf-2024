def repeat(n_times):
    def repeat_decorator(f):
        def wrapper(*args, **kwargs):
            res = f(*args, **kwargs)
            for _ in range(n_times - 1):
                f(*args, **kwargs)
            return res

        return wrapper

    return repeat_decorator


@repeat(n_times=5)
def dummy_func():
    print('ya tuta')


if __name__ == '__main__':
    dummy_func()
